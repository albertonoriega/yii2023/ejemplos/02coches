<?php

namespace app\controllers;

use app\models\Coche;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CocheController implements the CRUD actions for Coche model.
 */
class CocheController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Coche models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Coche::find(),

            'pagination' => [
                'pageSize' => 5
            ],
            'sort' => [
                'defaultOrder' => [
                    'bastidor' => SORT_DESC,
                ]
            ],

        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'foto1' => '@web/imgs/foto1',
            'foto2' => '@web/imgs/foto2',
        ]);
    }

    /**
     * Displays a single Coche model.
     * @param string $bastidor Bastidor
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($bastidor)
    {
        return $this->render('view', [
            'model' => $this->findModel($bastidor),
        ]);
    }

    /**
     * Creates a new Coche model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Coche();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'bastidor' => $model->bastidor]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Coche model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $bastidor Bastidor
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($bastidor)
    {
        $model = $this->findModel($bastidor);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'bastidor' => $model->bastidor]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Coche model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $bastidor Bastidor
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($bastidor)
    {
        $this->findModel($bastidor)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Coche model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $bastidor Bastidor
     * @return Coche the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($bastidor)
    {
        if (($model = Coche::findOne(['bastidor' => $bastidor])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('No se encuentra el valor introducido');
    }

    public function actionMostrar()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Coche::find()
        ]);

        return $this->render('mostrar', [
            'dataProvider' => $dataProvider,
        ]);
    }
}
