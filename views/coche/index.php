<?php

use app\models\Coche;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Listado de coches';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="coche-index">

    <div class="cabecera">

        <?= Html::img("{$foto1}.jpeg", ['width' => 200]) ?>
        <h1><?= Html::encode($this->title) ?></h1>
        <?= Html::img("{$foto2}.jpeg", ['width' => 200]) ?>

    </div>

    <p class="crear">
        <?= Html::a('<i class="fas fa-car"></i> Añadir coche <i class="fas fa-car"></i>', ['create'], ['class' => 'botonCrear']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'bastidor',
            'marca',
            'modelo',
            'cilindrada',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Coche $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'bastidor' => $model->bastidor]);
                }
            ],
        ],
        'tableOptions' => ['class' => 'tablaGrid'],
    ]); ?>


</div>