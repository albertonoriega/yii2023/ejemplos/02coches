<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\Coche $model */

$this->title = $model->bastidor;
$this->params['breadcrumbs'][] = ['label' => 'Coches', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="coche-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'bastidor' => $model->bastidor], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Eliminar', ['delete', 'bastidor' => $model->bastidor], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Estás seguro que quieres eliminar el registro?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'bastidor',
            'marca',
            'modelo',
            'cilindrada',
        ],
    ]) ?>

    <!--<div> Bastidor : <?= $model->bastidor ?></div> -->
</div>