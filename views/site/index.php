<?php

/** @var yii\web\View $this */

use yii\helpers\Html;

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron text-center bg-transparent mt-5 mb-5">
        <h1 class="display-4">Gestión de coches</h1>

        <p class="lead">Aplicación de un concesionario</p>
    </div>
    <div class="fotoIndex">
        <?php
        echo Html::img('@web/imgs/foto3.jpeg');
        ?>
    </div>
</div>